class Atsa < Formula
  desc "A command line tool for sound analysis according to the ATS spectral modeling system"
  homepage "https://dxarts.washington.edu/wiki/analysis-transformation-and-synthesis-ats"
  url "https://gitlab.com/dxarts/projects/ats/atsa.git", :branch => "main"
  version "1.0"
  sha256 ""
  license "TCL"

  def install
    system "./configure", "--prefix=#{prefix}"
    system "make install"
  end

  test do
    system "#{bin}/atsa"
  end
end
