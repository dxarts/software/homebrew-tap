# DXARTS homebrew formulae

This repository contains miscellaneous [homebrew](https://brew.sh/) formulae. 

First, add this tap to homebrew:
```
brew tap dxarts/tap https://gitlab.com/dxarts/software/homebrew-tap.git
```

Then install formulae from this tap:
```
brew install dxarts/tap/<formula-name>
```

### List of formulae
- [atsa](https://dxarts.washington.edu/wiki/analysis-transformation-and-synthesis-ats) - spectral modeling tool
